var express = require('express'),
	router = express.Router({
		'strict': true,
	}),
	template = 'index',
    http = require('http'),
	data,
	err,
	renderTemplate = require('./utils/renderTemplate'),
	request = require('request');

var notImplemented = function(next) {
	err = new Error('Not Implemented');
	err.status = 501;
	next(err);
};

var badRequest = function(next) {
	err = new Error('Bad Request');
	err.status = 400;
	next(err);
};

var matchParamPattern = function(a, b, next) {
	if (!a.match(b)) {
		badRequest(next);
	}
	next();
};

router.use(function(req, res, next) {
	// match host header
	// var pattern = '(amp\.)\w+\.com';
	// console.log(req.hostname);
	// console.log(req.headers.host);
	// matchParamPattern(req.hostname, pattern, next);
	next();
});
// 
// router.param('channelSlug', function(req, res, next) {
// 	var channelSlug = req.params.channelSlug,
// 		pattern = new RegExp('[-a-zA-Z0-9]+');
// 	matchParamPattern(channelSlug, pattern, next);
// });
//
// router.param('year', function(req, res, next) {
// 	var year = req.params.year,
// 		pattern = new RegExp('^[0-9]{4}$');
// 	matchParamPattern(year, pattern, next);
// });
//
// router.param('month', function(req, res, next) {
// 	var month = req.params.month,
// 		pattern = new RegExp('^[0-9]{2}$');
// 	matchParamPattern(month, pattern, next);
// });

router.route('/')
	.get(function(req, res, next) {
		data = {
			template: 'index',
			title: 'Couples',
			copy: 'What happened?',
			categories: [{
					id: 0,
					value: 'emotional'
				}, {
					id: 1,
					value: 'financial'
				}, {
					id: 2,
					value: 'sexual'
				}, {
					id: 3,
					value: 'communication'
				}, {
					id: 4,
					value: 'decisions'
				}, {
					id: 5,
					value: 'spontanaity'
				}, {
					id: 6,
					value: 'habits'
				}, {
					id: 7,
					value: 'chores'
				}, {
					id: 8,
					value: 'family'
				}, {
					id: 9,
					value: 'hygiene'
				}
			],
			eventImpact: [{
					id: 1,
					value: 'low'
				},{
					id: 2,
					value: 'medium'
				},{
					id: 3,
					value: 'high'
				}
			],
			path: req.path
		};
		renderTemplate(res, data);
	})
	.post(function(req, res, next) {
		request.post({
			url: 'http://54.213.192.121/api/v1/addaction',
			form: req.body
		}, function(error, res, body) {
			console.log(body);

            data = {
    			template: 'feed',
                title: 'Couples'
            };

    		renderTemplate(res, data);
		});
	});

	router.route('/register')
		.get(function(req, res, next) {
			data = {
				template: 'register',
				title: 'Couples',
				copy: 'Sign up now!',
				path: req.path
			};
			renderTemplate(res, data);
		})
		.post(function(req, res, next) {
			request.post({
				url: 'http://54.213.192.121/api/v1/createuser',
				form: req.body
			}, function(error, response, body) {
				console.log(body);
			});
		});

module.exports = router;
