var express = require('express'),
    jade = require('jade'),
    path = require('path'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    favicon = require('serve-favicon'),
    host = null,
    port = null,
    cors = require('cors'),
    app = express();

app.use("/public", express.static(path.join(__dirname, '../public')));

app.set('views', path.join(__dirname, '../views'));

app.use(favicon(__dirname + '/../public/favicon.ico'));

app.set('view engine', 'jade');

app.set('env', 'development');

app.use(morgan('dev'));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

/**
 * Allow CORS
 */
app.use(cors());

app.set('ipAddress', 'localhost');
app.set('port', 5000);

var routes = require('../src/routes');

app.use('/', routes);
// app.put('*', notImplemented())
// app.delete('*', notImplemented())

// catch errors
app.use(function(err, req, res, next) {
    res.status(err.status || 500);

    res.render('error', {
        title: err.message,
        message: err.status + ' ' + err.message,
        error: err
    });
});

var server = app.listen(app.get('port'), app.get('ipAddress'), function() {
    console.log('listening on http://%s:%s', app.get('ipAddress'), app.get('port'));
});

module.exports = app;
