var renderTemplate = function(r, d) {
    r.render(d.template, {
        // page title
        title: d.title,
        // path of request
        path: d.path,
        copy: d.copy,
        categories: d.categories,
        eventImpact: d.eventImpact,
        // pass in styles as string or other to template
        style: 'body { background-color: #eee; color: #f00; }',
        schema: {
            "@context": "http://schema.org",
            "@type": "Article",
            "headline": d.title,
            "description": 'description',
            "datePublished": 'datapublished',
            "image": "image"
        }

    });
};

module.exports = renderTemplate;
