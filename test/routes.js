(function() {
	'use strict';

	var request = require('supertest');
	var server = require('../src/app');

	describe('testing routes', function() {

		it('GET home route - responds with html', function(done) {
			request(server)
				.get('/')
				.expect('Content-type', /html/)
				.expect(200, done);
		});

		it('POST / - responds with 200', function(done) {
			request(server)
				.post('/')
				.expect(200)
				.end(function(err, res){
					console.log('res', res)
					console.log('err', err)
					done();
				});
		});

		it('PUT / - responds with 501', function(done) {
			request(server)
				.put('/')
				.expect(501, done);
		});

		it('DELETE / - responds with 501', function(done) {
			request(server)
				.delete('/')
				.expect(501, done);
		});

	});
})();
