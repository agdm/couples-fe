/*

Gruntfile for www.complex.com
By: Brandon Nydell & Paul Lewis

See the botttom of this gruntfile on the basics of how to use the grunt.

*/

// Prevent memory leak warnings
require('events').EventEmitter.prototype._maxListeners = 100;


var path = require('path'),
	escapeChar = process.platform.match(/^win/) ? '^' : '\\',
	cwd = process.cwd().replace(/( |\(|\))/g, escapeChar + '$1');

module.exports = function(grunt) {

	// #### Load all grunt tasks
	// Find all of the task which start with `grunt-` and load them, rather than explicitly declaring them all.
	require('matchdep').filter(['grunt-*', '!grunt-cli']).forEach(grunt.loadNpmTasks);

	// Project configuration.
	var icfg = {

		// Read the package.json.
		pkg: grunt.file.readJSON('package.json'),

		// ### grunt-contrib-clean
		// Remove files specified.
		clean: {
			site: ['doc', '**/DOC.md', '**/DOCHA.md'] // Generated Documentation Files.
		}

		// ### grunt-contrib-concat
		// Concat files into a single file
		,
		concat: {
			"node-doc": {
				src: [
					'README.md', 'TOC.md', 'DOCHA.md'
				],
				dest: 'DOC.md',
				options: {
					process: function(src, filepath) {
						console.log(filepath);
						if (/DOCHA.md$/.test(filepath)) {
							return '# Tests:\n' + src.replace(/^#/, '##');
						}
						return src;
					}
				}
			}
		},

		// ### grunt-concurrent
		// Runs tasks concurrently to speed things up.
		// concurrent: {
		// 	"build1": [
		// 		'sass', 'browserify', 'html2js'
		// 	],
		// 	"build2": [
		// 		'postcss', 'concat'
		// 	],
		// 	'build3': [
		// 		'cssmin', 'uglify'
		// 	]
		// },

	};

	try {
		require('matchdep').filterDev(['grunt-*', '!grunt-cli']).forEach(grunt.loadNpmTasks);
		var docha = require('docha');
		// ### grunt-contrib-watch
		icfg.watch = {
			options: {
				reload: false
			}
		};

		icfg["ddescribe-iit"] = {
			node: ['test/**/*.js']
		};

		// ### grunt-contrib-jshint
		// Lint JS for errors. Rules are in .jshintrc in the root folder.
		icfg.jshint = {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			}
            , application: [
				'src/**/*.js'
            ],
			gruntfile: [
				'Gruntfile.js'
			]
		};

		// ### grunt-jsdoc
		// Create Documentation from the JS automagically.
		icfg.jsdoc = {
			node: {
				src: ['DOC.md', 'src/**/*.js'],
				options: {
					destination: 'doc'
				}
			}
		};

		// ### grunt-apidoc
		// Generate API Documentation from the Node files.
		icfg.apidoc = {
			node: {
				src: [
					".",
				],
				dest: "doc/api",
				options: {
					excludeFilters: [
						'node_modules/'
					]
				}
			}

		}

		icfg.mochaTest = {
				test: {
					src: ['test/**/*.js']
				}
			}

			// ### grunt-mocha-istanbul
			// Generate coverage documentation.
		icfg.mocha_istanbul = {
			coverage: {
				src: 'test', // a folder works nicely
				options: {
					reportFormats: ['html'],
					coverageFolder: 'doc/coverage'
				}
			}
		};


		// ### grunt-contrib-copy
		// Copy the image file into the site.
		icfg.copy = {
			// main: {
			// 	files: {
			// 		'doc/keymaster.png': 'keymaster.png'
			// 	}
			// }
		};
		icfg.docha = {
			node: {
				"testPath": "test/**/*.js",
				output: "DOCHA.md",
				code: false,
				escape: []
			}
		}

	} catch (e) {

		console.log("/**************************************");
		console.log(" * Production Configuration Detected. *");
		console.log(" **************************************/");
		throw(e);

	}

	grunt.initConfig(icfg);

	// grunt validate
	// Task to lint both JS and css
	grunt.registerTask('validate', ['jshint']);

	grunt.registerTask('site', [
		'docha', 'concat:node-doc', 'jsdoc', 'apidoc', 'mocha_istanbul', 'copy'
	]);
	grunt.registerTask('test', [
		'mochaTest'
	]);
	grunt.registerMultiTask('docha', 'Docha Task', function() {
		GLOBAL.inject = function() {
			return function() {};
		};

		var done = this.async();
		docha({
			testPath: this.data.testPath || "test/",
			output: this.data.output || "DOCHA.md",
			code: this.data.code || false,
			escape: this.data.escape || []
		}, function(error) {
			if (error) {
				console.error(error);
			}
			done(!error);
		});
	});
};
